public class Product {
  private String firstType;
  private String lastType;
  
  public Product() {
	  this.firstType = "Dress Pants";
	  this.lastType = "Dress Shirt";
  }
  
  public Product(String firstType) {
	  this.firstType = firstType;
	  this.lastType = "";	  
  }
  
  public Product (String firstType, String lastType) {
    this.firstType = firstType;
    this.lastType = lastType;
  }
  
  public String getFirstType() {return firstType;}
  public String getLastType() {return lastType;}

  public void setFirstType(String firstType) {
	    this.firstType = firstType;	  
  }
  public void setLastType(String lastType) {
	    this.lastType = lastType;	  
  }
  
  public String getProduct() {
    return firstType + " " + lastType;
  }

  @Override
  public String toString() {
    return "["+getProduct()+"]";
  }
}
